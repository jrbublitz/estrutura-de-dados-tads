package FILANDO_LISTA;

public interface Lista {
	
	void enqueue(int v) throws Exception;
	
	float dequeue() throws Exception;
	
	boolean isEmpty();
	
	void reset();
	
	
}	
