package FILANDO_LISTA;

public class Main {

	public static void main(String[] args) throws Exception {
	
		FilaLista lista = new FilaLista();
		
		System.out.println(" ... LISTA enqueue ... ");
		lista.enqueue(1);
		lista.enqueue(2);
		lista.enqueue(3);
		System.out.println(lista.toString());		
		
		System.out.println(" ... LISTA dequeue ... ");
		lista.dequeue();
		System.out.println(lista.toString());	
		
		if(lista.isEmpty() == true) {
			System.out.println(" ... LISTA vazia ... ");
		}else {
			System.out.println(" ...RESENTANDO LISTA ... ");
			lista.reset();
		}
		
		

	}

}
