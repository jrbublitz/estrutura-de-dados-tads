package FILANDO_LISTA;

public class FilaLista implements Lista {
	NoLista ini;
	NoLista fim;
	
	public FilaLista() {
		this.ini = null;
		this.fim = null;
	}
	
	public String toString() {
		String lista = "";
		
		if (isEmpty()) {
			return "Pilha vazia.";
		} else {
			for (NoLista p = ini; p != null; p = p.getProx()) {
				lista = lista + p.getInfo() + " ";
			}
		}
		return lista;
	}
	
	@Override
	public void enqueue(int v) throws Exception {
		NoLista enqueue = new NoLista();
		
		enqueue.setInfo(v);  
		enqueue.setProx(ini);
		ini = enqueue;
		
	}
	@Override
	public float dequeue() throws Exception {
		if(isEmpty()) {
			throw new Exception("A PILHA EST� VAZIA !");
		} else {
			int temp = ini.getInfo();
			ini = ini.getProx();
			return temp;			
		}	
	}
	
	@Override
	public boolean isEmpty() { return (ini == null); }
	
	@Override
	public void reset() { ini = null; }
}
